const URL_SERVIDOR = '127.0.0.1/Controlador/ControladorBackend.php';

/** Realiza la conexión asíncrona con PHP para ejecutar los servicios*/
function EjecutarAjaxJson(servicioWeb = '', parametros={}, callback) {
    parametros.servicioWeb = servicioWeb;
    $.ajax({
        data: parametros,
        url: URL_SERVIDOR,
        type: 'post',
        success: function(data, status) {
            callback(data);
        },
        error: function(error) {
            MostrarMensajeUsuario('Error', 'Problemas con el servidor', 'error');
            return;
        }
    });
}

/** Muestra un mensaje de alerta al usuario*/
function MostrarMensajeUsuario(titulo ='', mensaje='', icono='') {
    swal({
        title: titulo.toString(),
        text: mensaje.toString(),
        icon: icono.toString()
    });	
}

/** Obtiene la Fecha y hora actual en tiempo real */
function MostrarFechaHoraActual(IdElementoHtmlFecha='', IdElementoHtmlHora='') {  
    function formatTime(n) {
        return (n < 10) ? "0" + n : n;
    };
    function checkTime() {
    var today = new Date(),
		
        day = ["Dom","Lun", "Mar", "Mie", "Jue", "Vie", "Sáb"],
        month = ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"],
				
        h = formatTime(today.getHours()),
        min = formatTime(today.getMinutes()),
        seg = formatTime(today.getSeconds()),
        hour = h,
        w = "a.m.";
			
    if (hour >= 12) {
      hour = formatTime(hour - 12);
      w = "p.m.";
    };
		
    if (hour == 0) {
      hour = 12;
    };
			
    document.getElementById(IdElementoHtmlFecha).innerHTML = "<span>" + day[today.getDay()] + ", " + today.getDate() + " " + month[today.getMonth()] + " " +  today.getFullYear() + "</span>";	
    document.getElementById(IdElementoHtmlHora).innerHTML = "<span class='hm-time'>" + hour + ":" + min + "</span> <span class='s-time'>" + seg + "</span> <span class='f-time'>" + w + "</span>";
		
    var d = setTimeout(function() {
      checkTime()
    }, 500);
  };
  checkTime();
}

























