
$('#btnVerificarDatos').on('click', function () {
    var HoraInicio      = $("#txtHoraInicio").val(),
    HoraTermino         = $("#txtHoraTermino").val(),
    FechaActual         = $("#txtFechaActual").val(),
    TipoMantenimiento   = $("#txtTipoMantenimiento").val(),
    Marca               = $("#txtMarca").val(),
    Placa               = $("#txtPlaca").val(),
    ModeloVehiculo      = $("#txtModeloVehiculo").val(),
    ModeloCarroceria    = $("#txtModeloCarroceria").val(),
    ColorVehiculo       = $("#txtColorVehiculo").val(),
    Kilometraje         = $("#txtKilometraje").val(),
    Sede                = $("#txtSede").val(),
    DescripcionTRDV     = $("#txtDescripcionTRDV").val(),
    DescripcionTR       = $("#txtDescripcionTR").val(),
    DescripcionRAU      = $("#txtDescripcionRAU").val(),
    DescripcionST       = $("#txtDescripcionST").val(),
    ObservacioGenerales = $("#txtObservacionesGenerales").val(),
    NombreRecepcion     = $("#txtNombreRecepcion").val(),
    DNIRecepcion        = $("#txtDNIRecepcion").val(),
    NombreEntrega       = $("#txtNombreEntrega").val(),
    DNIEntrega          = $("#txtDNIEntrega").val();

    if (HoraInicio.length == 0) {
        MostrarMensajeUsuario("Error", "Debe ingresar la hora de inicio", 'error');
        return;
    }
    if (HoraTermino.length == 0) {
        MostrarMensajeUsuario("Error", "Debe ingresar la hora de termino", 'error');
        return;
    }
    if (FechaActual.length == 0) {
        MostrarMensajeUsuario("Error", "Debe ingresar la fecha", 'error');
        return;
    }
    if (TipoMantenimiento.length == 0) {
        MostrarMensajeUsuario("Error", "Debe ingresar el tipo de mantenimiento", 'error');
        return;
    }
    if (Marca.length == 0) {
        MostrarMensajeUsuario("Error", "Debe ingresar la marca del vehículo", 'error');
        return;
    }
    if (Placa.length == 0) {
        MostrarMensajeUsuario("Error", "Debe ingresar la placa del vehículo", 'error');
        return;
    }
    if (ModeloVehiculo.length == 0) {
        MostrarMensajeUsuario("Error", "Debe ingresar el modelo del vehículo del vehículo", 'error');
        return;
    }
    if (ModeloCarroceria.length == 0) {
        MostrarMensajeUsuario("Error", "Debe ingresar el modelo de carrocería", 'error');
        return;
    }
    if (ColorVehiculo.length == 0) {
        MostrarMensajeUsuario("Error", "Debe ingresar el color del vehículo", 'error');
        return;
    }
    if (Kilometraje.length == 0) {
        MostrarMensajeUsuario("Error", "Debe ingresar el kilometraje", 'error');
        return;
    }
    if (Sede.length == 0) {
        MostrarMensajeUsuario("Error", "Debe seleccionar una sede", 'error');
        return;
    }
    if (isNaN(DNIRecepcion)) {
        MostrarMensajeUsuario("Error", "Debe ingresar solo números en el DNI", 'error');
        return;
    }
    if (isNaN(DNIEntrega)) {
        MostrarMensajeUsuario("Error", "Debe ingresar solo números en el DNI", 'error');
        return;
    }
       
});
