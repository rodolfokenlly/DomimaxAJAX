<?php
include_once 'VehiculosData.php';

class Router {

    private $Instancia_principal;

    public function __construct() {
        $this->Instancia_principal = new VehiculosData();
    }

    /** Retorna la lista total de OTNS*/
    static public function SelectListadoOTN($callback) {
        $resultadoRegistros = self::$Instancia_principal->listaRegistros();
        if (strlen($resultadoRegistros) == 0 || $resultadoRegistros == false) {
            $resultadoRegistros = [];
            $callback($resultadoRegistros);
        }

        $callback($resultadoRegistros);
    }//Test:OK

    /** Retorna un registro OTN individual específico*/
    static public function SelectIndividualOTN($otn,$callback) {
        $resultadoRegistros = self::$Instancia_principal->listaRegistroIndividual($otn);
        if (strlen($resultadoRegistros) == 0 || $resultadoRegistros == false) {
            $resultadoRegistros = [];
            $callback($resultadoRegistros);
        }

        $callback($resultadoRegistros);
    }//Test:OK

    /** Inserta un registro otn completo y retorna el resultado total de la lista OTNS*/
    static public function InsertOTN($registro,$callback) {
        $resultado = self::$Instancia_principal->agregaRegistro($registro);
        if (strlen($resultado) == 0 || $resultado == false) {
            $resultado = [];
            $callback($resultado);
        }

        $resultadoRegistro_ = self::$Instancia_principal->listaRegistros();
        if (strlen($resultadoRegistro_) == 0 || $resultadoRegistro_ == false) {
            $resultadoRegistro_ = [];
            $callback($resultadoRegistro_);
        }
    
        $callback($resultadoRegistro_);
    }//Test:OK

    /** Actualiza un registro OTN individual y retorna la lista total de OTNS*/
    static public function UpdateOTN($registro,$callback) {
        $resultado = self::$Instancia_principal->actualizaRegistro($registro);
        if (strlen($resultado) == 0 || $resultado == false) {
            $resultado = [];
            $callback($resultado);
        }

        $resultadoRegistro_ = self::$Instancia_principal->listaRegistros();
        if (strlen($resultadoRegistro_) == 0 || $resultadoRegistro_ == false) {
            $resultadoRegistro_ = [];
            $callback($resultadoRegistro_);
        }
    
        $callback($resultadoRegistro_);
    }//Test:OK

    /** Retorna una lista de registros OTNS de un rango de fechas dadas*/
    static public function SelectRangoFechas($fecha_desde,$fecha_hasta,$callback) {
        $resultadoRegistro = self::$Instancia_principal->buscarRangoFechas($fecha_desde,$fecha_hasta);
        if (strlen($resultadoRegistro) == 0 || $resultadoRegistro == false) {
            $resultadoRegistro = [];
            $callback($resultadoRegistro);
        }

        $callback($resultadoRegistro);
    }//Test:OK

    /** Retorna un registro OTN en base al número de placa*/
    static public function SelectPlacaIndividual($placa,$callback) {
        $resultadoRegistro = self::$Instancia_principal->buscarPlaca($placa);
        if (strlen($resultadoRegistro) == 0 || $resultadoRegistro == false) {
            $resultadoRegistro = [];
            $callback($resultadoRegistro);
        }

        $callback($resultadoRegistro);
    }//Test:OK

    /** Elimina un registro otn*/
    static public function EliminarOTN($otn,$callback) {
        $resultadoRegistro = self::$Instancia_principal->eliminarRegistro($otn);
        if (strlen($resultadoRegistro) == 0 || $resultadoRegistro == false) {
            $resultadoRegistro = [];
            $callback($resultadoRegistro);
        }

        $resultadoListado = self::$Instancia_principal->listaRegistros();
        if (strlen($resultadoListado) == 0 || $resultadoListado == false) {
            $resultadoListado = [];
            $callback($resultadoListado);
        }

        $callback($resultadoListado);
    }//Test:OK
}

?>