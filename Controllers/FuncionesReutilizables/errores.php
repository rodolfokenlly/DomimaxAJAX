<?php
include_once 'SwitAlert.php';

$errores_listado = array
(
	'err_otn' => mensajesSwitAlert('Error', 'El OTN es incorrecto!', 'error'),
	'err_server' => mensajesSwitAlert('Error', 'Lo sentimos, tenemos problemas con el servidor.', 'error'),
	'err_buscar_placa' => mensajesSwitAlert('Error', 'Lo sentimos, no existen registros de dicha Placa.', 'error'),
	'err_placa_incorrecta' => mensajesSwitAlert('Error', 'La placa ingresada es incorrecta.', 'error')
);

$errores_editar = array
(
'err_otn_diferentes' => mensajesSwitAlert('Error', 'No puedes modificar el OTN', 'error'),
'err_kilometraje' => mensajesSwitAlert('Error', 'El kilometraje debe ser un número menor a 8 dígitos. No coloques letras.', 'error'),
'err_dni_s' =>  mensajesSwitAlert('Error', 'El DNI solo puede contener números.', 'error'),
'err_actualizar' => mensajesSwitAlert('Error', 'Lo sentimos, no se puede actualizar', 'error'),
);

$errores_agregar = $errores_editar;
$errores_agregar_adicional =  mensajesSwitAlert('Error', 'Lo sentimos, no se puede agregar el registro .Tenemos problemas con el servidor.', 'error');
$ok_agregar =  mensajesSwitAlert('Excelente!', 'Se agrego el registro exitosamente!', 'success');
$regresar = '<script>window.history.back();</script>';
$editar_ok = mensajesSwitAlert('Excelente!', 'Se actualizó el registro exitosamente!', 'success');
$regresar_inicio = "<meta http-equiv=Refresh content=\"2 ; url=../vistas/listado.php\">";
$error_busqueda_fecha = mensajesSwitAlert('Advertencia!', 'No se encontraron registros de dichas fechas!', 'warning');
$eliminado_ok = mensajesSwitAlert('Excelente!', 'Se elimino correctamente el registro.', 'success');
$eliminado_error = mensajesSwitAlert('Error', 'No se puede eliminar el registro porque tiene asociada una factura.', 'error');
$eliminado_oksi = "<meta http-equiv=Refresh content=\"2 ; url=../vistas/listado.php\">";
$error_proforma =  mensajesSwitAlert('Busqueda erronea', 'Recuerda colocar al comienzo la letra P si es por número de proforma', 'error');
$error_limite = mensajesSwitAlert('Error', 'el limite es incorrecto', 'error');
$error_fecha_vacia =  mensajesSwitAlert('Error', 'Las fechas a consultar no pueden estar vacías', 'error');
$error_busqueda_vacia = mensajesSwitAlert('Error', 'Debe escribir el ot-n o placa a buscar!', 'error');
function mensajesSwitAlert($titulo, $mensaje, $icono) {
	$dato = '<script>
		swal({
			title: "'.$titulo.'",
			text: "'.$mensaje.'",
			icon: "'.$icono.'"
		});	
	</script>';

	return $dato;
}
?>
