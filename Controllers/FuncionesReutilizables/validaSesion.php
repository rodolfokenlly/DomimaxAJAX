<?php
session_start();
?>

<!DOCTYPE html>
<html>
<head>
    <title>Validando Usuario</title>
</head><body>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> 
<?php
if (isset($_POST['validar'])) {
  require_once '../modelos/VehiculosData.php';
  $user=$_POST['user'];
  $pass=$_POST['contra'];

  $Usuario = new VehiculosData();
  $respuesta = $Usuario -> buscaUsuario($user, $pass);

  if ($Usuario -> codigo_retorno == '00') {
    $_SESSION['user'] = $respuesta[0]['nombre_usuario'];
    $_SESSION['name_user'] = $respuesta[0]['nombre_apellidos'];
    $mensaje = "Iniciando sesion para". $_SESSION['name_user']; 
    $mensaje = strtoupper($mensaje);
    print '<script> window.location="../vistas/listado.php";</script>';
  }
  else {
    $error = mensajesSwitAlert("Error", "Usuario o contrasenia incorrecta.", "error");
    $regresar_inicioo = "<meta http-equiv=Refresh content=\"2 ; url=../index.php\">";
    print $error;
    print $regresar_inicioo;
  }
}
else {
  print 'No tienes acceso al sistema, inicia sesion';
}
?> 
<?php
function mensajesSwitAlert($titulo, $mensaje, $icono) {
	$dato = '<script>
		swal({
			title: "'.$titulo.'",
			text: "'.$mensaje.'",
			icon: "'.$icono.'"
		});	
	</script>';

	return $dato;
}
?>
</body>
</html>
