<?php
include_once '../Models/VehiculosData.php';
include_once '../Models/Router.php';
include_once '/FuncionesReutilizables/calculaFecha.php';
include_once '/FuncionesReutilizables/fechaBD.php';
include_once '/FuncionesReutilizables/fechaWeb.php';

$Instancia_vehiculo = new VehiculosData();

$servicio = $_POST['servicioWeb'];

switch ($servicio) {
    case 'SelectListadoOTN':

        Router::SelectListadoOTN(function ($listado) {
            if (strlen($listado) == 0) {
                $devolucion = json_encode(['error'=> 'Sin datos del servidor']);
                echo $devolucion;
            }
            
            $devolucion = json_encode($listado);
            echo $devolucion;
        });

    break;

    case 'SelectIndividualOTN':
        $otn = $_POST['nregistro_otn'];
        
        Router::SelectIndividualOTN($otn, function ($resultado) {
            if (strlen($resultado) == 0) {
                $devolucion =  json_encode(['error'=> 'no existen datos del otn']);
                echo $devolucion;
            }

            $devolucion = json_encode($resultado);
            echo $devolucion;
        });
    break;

    case 'InsertOTN':
        $registro = $_POST['datos_otn'];
        
        Router::InsertOTN($registro, function ($listado) {
            if (strlen($listado) == 0) {
                $devolucion = json_encode(['error'=>'no se pudo insertar el registro']);
                echo $devolucion;    
            }
            
            $devolucion = json_encode($listado);
            echo $devolucion;
        });
    break;

    case 'UpdateOTN':
        $registro = $_POST['datos_otn'];
        
        Router::UpdateOTN($registro, function ($listado) {
            if (strlen($listado) == 0) {
                $devolucion = json_encode(['error'=>'no se pudo actualizar el registro']);
                echo $devolucion;    
            }
            
            $devolucion = json_encode($listado);
            echo $devolucion;
        });
    break;

    case 'SelectRangoFechas':
        $fecha_desde = $_POST['fecha_desde'];
        $fecha_hasta = $_POST['fecha_hasta'];
        
        Router::SelectRangoFechas($fecha_desde, $fecha_hasta, function ($resultado) {
            if (strlen($resultado) == 0) {
                $devolucion = json_encode(['error'=>'no existen registro de las fechas dadas']);
                echo $devolucion;
            }
            
            $devolucion = json_encode($resultado);
            echo $devolucion;
        });        
    break;

    case 'SelectPlacaIndividual':
        $placa = $_POST['placa'];
        
        Router::SelectPlacaIndividual($placa, function ($resultado) {
            if (strlen($resultado) == 0) {
                $devolucion = json_encode(['error'=>'no existen registros de dicha placa']);
                echo $devolucion;    
            }
            
            $devolucion = json_encode($resultado);
            echo $devolucion;
        });
    break;

    case 'EliminarOTN':
        $otn = $_POST['nregistro_otn'];
        
        Router::EliminarOTN($otn, function ($listado) {
            if (strlen($listado) == 0) {
                $devolucion = json_encode(['error'=>'no se pudo eliminar el registro']);
                echo $devolucion;    
            }
            
            $devolucion = json_encode($listado);
            echo $devolucion;
        });
    break;
    default:
        $devolucion = json_encode(['error'=>'ERROR LLAMANDO AL SERVIDOR']);
        echo $devolucion;
    break;
}

?>